#include <stdio.h>
#include <stdlib.h>
#include <String.h>

//se crea la estructura empleado con atributos y una funcion de puntere
struct employee{
	int experience;
	double basic_salary;
	double salary;
	char name[20];
	int (*calculate_salary) (int a); // funcion de puntero
	
};

//se crea el tipo de la estrucutura
typedef struct employee emp;
typedef emp *Employee; // se crea el puntero para trabajar

//se crean dos funciones una es calcular salario
//el promer metodo acepta una variable de tipo apuntador
//de acuero a la experencia es el salario que percibe el empleado
int cal_salary(Employee exp_em){
	int salary = 0;
			if(exp_em->experience>15)
				salary +=25000;
			else if(exp_em->experience>10)
				salary+=15000;
			else if(exp_em->experience>5)
				salary+=5000;
		return salary;
			
}

//
	int cal_salary_cop(int exp){
int salary=0;
		if(exp>15)
			salary+=25000;
		else if(exp>10)
			salary+=15000;
		else if(exp>5)
			salary+=5000;
	return salary;
	}
	
	//aqui se crean los objetos para los atributos que se van a trabajar
	//asi como se crea una funci�n para empleado
	Employee new_Employee(int experience, int basic_salary){
		Employee obj=(Employee)malloc(sizeof(emp)); //malloc funcion para asignar memoria al objeto que esta almacenado en el puntero emp 
		obj->basic_salary=basic_salary;
		obj->experience=experience;
		obj->calculate_salary=cal_salary_cop(experience); // aqui se esta haciendo referencia al puntero creado en la estrucgtura empleado y se esta pasando la exmperiencia
		return obj; //devuelve al objeto creado
	}
	
	//esto sirve para limpiar los obj que se crean 
	void destroy_Employee(Employee e){
		free(e);
	}
	
	int main()
{
		//aqui se crean dos objetos de timpo empleado, asignandole experencia y salario
		Employee emp1=new_Employee(14,2000);
		Employee emp2=new_Employee(7,1500);
		//se tiene que hacer referencia a los objetos y los invocamos
		int emp1_sal=emp1->basic_salary;
		int emp2_sal=emp2->basic_salary;
		// se asgna la variable al metodo que se creo con anterioridad
		int emp1_cal_salary=cal_salary(emp1);
		int emp2_cal_salary=cal_salary(emp2);
		
		
		// se imprime lo que realiza el metodo 
		printf("Emp1 salario basico %d\n", emp1_sal);
		printf("Emp2 salario basico %d\n", emp2_sal);
		printf("Emp1 salario calculado %d\n", emp1_cal_salary);
		printf("Emp2 salario calculado %d\n", emp2_cal_salary);
		
		//se crea el objeto que apunte a la funcion calculate_salary
		/*emp1_cal_salary=emp1->calculate_salary;
		printf("Emp1 calculated salary like cop vay %d\n", emp1_cal_salary);
		emp1_cal_salary=emp2->calculate_salary;
		printf("Emp2 calculated salary like cop vay %d\n", emp2_cal_salary);*/
		
		destroy_Employee(emp1);
		destroy_Employee(emp2);
		
		return 0;
	}	
